package com.example.mservice.service2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Services {

	
	@GetMapping(value = "/hello")
	public String getMessage() {
		return "Hello from micro-service2";
	}
}
