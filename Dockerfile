FROM java:8
EXPOSE 3002
ADD /target/service2.jar service2.jar
ENTRYPOINT ["java","-jar","service2.jar"]